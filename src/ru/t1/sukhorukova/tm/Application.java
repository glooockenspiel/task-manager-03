package ru.t1.sukhorukova.tm;

import static ru.t1.sukhorukova.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
    }

    private static void processArguments(String[] args) {
        if (args == null || args.length == 0) {
            showError();
            return;
        }
        processArgument(args[0]);
    }

    private static void processArgument(String arg) {
        if (arg == null || arg.isEmpty()) {
            showError();
            return;
        }
        switch (arg) {
            case CMD_VERSION:
                showVersion();
                break;
            case CMD_ABOUT:
                showAbout();
                break;
            case CMD_HELP:
                showHelp();
                break;
            default:
                showError();
                break;
        }
    }

    private static void showError() {
        System.out.println("[ERROR]");
        System.out.println("This argument not supported...");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Victoria Sukhorukova");
        System.out.println("vsukhorukova@t1-consulting.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show version.\n", CMD_VERSION);
        System.out.printf("%s - Show about developer.\n", CMD_ABOUT);
        System.out.printf("%s - Show list arguments.\n", CMD_HELP);
    }

}
